Description
-----------
The module is intended to publish the ads, and marking them as a VIP.
Works with the "Notification of Payment of http://mobio.bg.
Dopalitelno need to install the module job_scheduler

Installation
Module is placed in a directory with additional modules sites/all/modules.
Consists of admin/build/modules
Installing module job_scheduler - http://drupal.org/project/job_scheduler.
It requires no settings.

Settings modules - admin/settings/sms_mobio/mobiobg

1. Sazdavame new type of ad content, or ask these settings
type of content available, which will use about
with Workflow / Options for publishing /
Without setting a bookmark for Posted / Publish / and
Introduced on the first page / promote /.
So user will be able to create ads, but they still
will not appear on the site.

2. Create and set service type "Notification of payment of
http://mobio.bg - for the Publishing of an advertisement. Ask for URL
http://vashiyat.sayt/check/sms/node. Create a second service from the same species
VIP announcement. VIP / service as a second / is another keyword and the same
settings.

3. Installation and setup module mobiobg. Requires installation of the module
job_scheduler - no settings for it. In settings set mobiobg
servID - number of service accounts mobio.bg. Set the number of days in which
ads will be displayed on site. Period is the same for Publication and VIP. This is
period is added for publication or VIP for every SMS sent to the ad.

To make ad published user sends SMS to short number of
WORD content 12, as word is the key word on the setting of the service
mobio.bg, while 12 is the id of the recorded announcement. Ad must be established before
Posted done.
To become a Featured Ad sent another SMS with the keyword of the second service and
id of the nodes.

*** Sending SMS VIP does not update Publish but makes it VIP.

Check the page admin/settings/mobiobg/debug.

It is imperative to have properly configured cron. This can be used
module drupal.org/project/poormanscron.

Module is incompatible with the module domain.

Maintainers
-----------
The Sms node module was originally developped by:
Svetoslav Stoyanov - http://drupal.org/user/717122, sve@d-support.eu
Georgi Baev - http://drupal.org/user/264047,

Description
-----------
Модула е предназначен за публикуване на обяви, и маркирането им като вип.
Работи с услугата "Известяване за плащане" на http://mobio.bg. 
Допълително е необходимо да се инсталира модула job_scheduler

Инсталиране
Модула се поставя в директорията с допълнителни модули sites/all/modules.
Включва се от admin/build/modules
Инсталира се модула job_scheduler - http://drupal.org/project/job_scheduler.
Той не се нуждае от настройки.

Настройки на модула - admin/settings/sms_mobio/mobiobg

1. Саздаваме нов вид съдържание обява, или задаваме тези настройки на
вид съдържание от наличните, който ще използваме за това,
с Workflow /Опции за публикуване/
настройки Без отметка за Публикувано /Publish/ и
Представен на първа страница /promote/.
Така потребителя ще може да създава обяви, но те все още
няма да се виждат на сайта.

2. Създаваме и настройваме услуга тип "Известяване за плащане" на
http://mobio.bg - за "Публикуване" на обява. За URL задаваме
http://вашият.сайт/check/sms/node. Създаваме втора услуга от същия вид
за ВИП на обява. Вип /като втора услуга/ е с друга ключова дума и същите
настройки.

3. Инсталираме и настройваме модула mobiobg. Изисква инсталирането на модула
job_scheduler - за него няма настройки. В настройките на mobiobg се задава
servID - номера на услугата от профила в mobio.bg. Задава се броя дни, в които
обявате ще се вижда на сайта. Периода е еднакъв за Публикуване и вип. Това е 
периода, който се добавя за Публикуване или Вип за всеки изпратен смс за обява.

За да стане обява Публикувана, потребителя изпраща смс на кратък номер със
съдържание ДУМА 12, като дума е ключовата дума от настройката на услугата в
mobio.bg, а 12 е id на записаната обява. Обявата трябва да е създадена преди да
се направи Публикувана.
За да стане Вип обява се изпраща друг смс с ключовата дума от втората услуга и
id на нода.

***Изпращането на смс за Вип Не прави обявата Публикувана, но я прави вип.

Проверяваме в страницата admin/settings/mobiobg/debug.

Задължително е да има правилно конфигуриран cron. За това може да се използва 
модула drupal.org/project/poormanscron.

Модула е несъвместим с модула domain.

Maintainers
-----------
The Sms node module was originally developped by:
Svetoslav Stoyanov - http://drupal.org/user/717122, sve@d-support.eu
Georgi Baev - http://drupal.org/user/264047, 